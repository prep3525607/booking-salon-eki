package com.booking.service;

public class MenuService {
    public static void mainMenu() {
        String[] mainMenuArr = {"Show Data", "Create Reservation", "Complete/cancel reservation", "Exit"};
        String[] subMenuArr = {"Recent Reservation", "Show Customer", "Show Available Employee", "Show Reservation History + Total Profit", "Back to main menu"};
    
        int optionMainMenu;
        int optionSubMenu;

        PrintService consoleUI = new PrintService();

        do {
            PrintService.printMenu("Main Menu", mainMenuArr);
            optionMainMenu = ValidationService.inputRange(0, 3);
            switch (optionMainMenu) {
                case 1:
                    do {
                        PrintService.printMenu("Show Data", subMenuArr);
                        optionSubMenu = ValidationService.inputRange(0, 4);
                        // Sub menu - menu 1
                        switch (optionSubMenu) {
                            case 1:
                                // panggil fitur tampilkan recent reservation
                                consoleUI.showRecentReservation(ReservationService.reservationList);
                                break;
                            case 2:
                                // panggil fitur tampilkan semua customer
                                consoleUI.showAllCustomer();
                                break;
                            case 3:
                                // panggil fitur tampilkan semua employee
                                consoleUI.showAllEmployee();
                                break;
                            case 4:
                                // panggil fitur tampilkan history reservation + total keuntungan
                                consoleUI.showHistoryReservation(ReservationService.reservationList);
                                break;
                            case 0:
                        }
                    } while (optionSubMenu != 0);
                    break;
                case 2:
                    // panggil fitur menambahkan reservation
                    ReservationService.createReservation();
                    break;
                case 3:
                    // panggil fitur mengubah workstage menjadi finish/cancel
                    ReservationService.editReservationWorkstage();
                    break;
                case 0:
                    System.out.println("Aplikasi Berakhir. Sampai Jumpa!");
                    break;
            }
        } while (optionMainMenu != 0);
		
	}
}
