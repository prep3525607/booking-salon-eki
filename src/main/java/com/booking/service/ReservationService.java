package com.booking.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.repositories.PersonRepository;
import com.booking.repositories.ServiceRepository;
import com.booking.models.Service;

public class ReservationService {
    static PrintService consoleUI = new PrintService();
    public static List<Reservation> reservationList = new ArrayList<>();

    public static void createReservation() {
        List<Person> persons = PersonRepository.getAllPerson();

        List<Customer> customers = persons.stream()
                .filter(person -> person instanceof Customer)
                .map(person -> (Customer) person)
                .collect(Collectors.toList());

        List<Employee> employees = persons.stream()
                .filter(person -> person instanceof Employee)
                .map(person -> (Employee) person)
                .collect(Collectors.toList());

        List<Service> services = ServiceRepository.getAllService();

        // Tampilkan daftar pelanggan
        System.out.println("Daftar Pelanggan:");
        consoleUI.showAllCustomer();
        System.out.print("Silahkan Masukkan Customer Id: ");
        String customerId = ValidationService.validateStringInput();
        Customer selectedCustomer = findCustomerById(customers, customerId);
        if (selectedCustomer == null) {
            System.out.println("Customer yang dicari tidak tersedia");
            return;
        }

        // Tampilkan daftar karyawan
        System.out.println("\nDaftar Karyawan:");
        consoleUI.showAllEmployee();
        System.out.print("Silahkan Masukkan Employee Id: ");
        String employeeId = ValidationService.validateStringInput();
        Employee selectedEmployee = findEmployeeById(employees, employeeId);
        if (selectedEmployee == null) {
            System.out.println("Employee yang dicari tidak tersedia");
            return;
        }

        // Pilih Serivce
        List<Service> selectedServices = selectServices(services);
        if (selectedServices.isEmpty() || selectedServices == null) {
            System.out.println("Service yang dicari tidak tersedia");
            return;
        }

        // Hitung total biaya reservasi
        double totalCost = calculateTotalCost(selectedServices);

        // Buat objek Reservation
        Reservation newReservation = Reservation.builder()
                .reservationId(ValidationService.getUniqueID("Rsv-"))
                .customer(selectedCustomer)
                .employee(selectedEmployee)
                .services(selectedServices)
                .reservationPrice(totalCost)
                .workstage("In process")
                .build();

        // Hitung harga reservasi berdasarkan jenis membership
        double priceMembership = newReservation.calculateReservationPrice();
        newReservation.setReservationPrice(priceMembership);

        // Tambahkan objek Reservation ke dalam list
        reservationList.add(newReservation);

        // Tampilkan total biaya dan pesan berhasil
        System.out.println("\nBooking Berhasil!");
        System.out.println("Total Biaya Booking: Rp." + priceMembership);
    }

    private static Customer findCustomerById(List<Customer> customers, String id) {
        for (Customer customer : customers) {
            if (customer.getId().equals(id)) {
                return customer;
            }
        }
        return null;
    }

    private static Employee findEmployeeById(List<Employee> employees, String id) {
        for (Employee employee : employees) {
            if (employee.getId().equals(id)) {
                return employee;
            }
        }
        return null;
    }

    private static List<Service> selectServices(List<Service> services) {
        List<Service> selectedServices = new ArrayList<>();
        boolean continueSelecting = true;
        
        while (continueSelecting) {
            System.out.println("\nDaftar Layanan:");
            displayServices(services);
            System.out.print("Silahkan Masukkan Service Id: ");
            String serviceId = ValidationService.validateStringInput();
            Service selectedService = findServiceById(services, serviceId);
            
            if (selectedService != null) {
                // Memeriksa apakah serviceId sudah dipilih sebelumnya
                if (selectedServices.stream().anyMatch(service -> service.getServiceId().equals(serviceId))) {
                    System.out.println("Service sudah dipilih");
                } else {
                    selectedServices.add(selectedService);
                    System.out.print("Ingin pilih service yang lain (Y/T)? ");
                    String choice = ValidationService.validateStringInput();
                    continueSelecting = choice.equalsIgnoreCase("Y");
                }
            } else {
                System.out.println("Service yang dicari tidak tersedia");
            }
        }
        return selectedServices;
    }
    

    private static void displayServices(List<Service> services) {
        System.out.println("+================================================================+");
        System.out.printf("| %-4s | %-10s | %-25s | %-14s |\n",
                "No.", "ID", "Nama Layanan", "Harga");
        System.out.println("+================================================================+");
        for (int i = 0; i < services.size(); i++) {
            Service service = services.get(i);
            System.out.printf("| %-4d | %-10s | %-25s | Rp%-12.2f |\n",
                    i + 1, service.getServiceId(), service.getServiceName(), service.getPrice());
        }
        System.out.println("+================================================================+");
    }

    private static Service findServiceById(List<Service> services, String id) {
        for (Service service : services) {
            if (service.getServiceId().equals(id)) {
                return service;
            }
        }
        return null;
    }

    private static Reservation findReservationById(List<Reservation> reservationList, String id) {
        for (Reservation reservation : reservationList) {
            if (reservation.getReservationId().equals(id)) {
                return reservation;
            }
        }
        return null;
    }

    private static int calculateTotalCost(List<Service> selectedServices) {
        int totalCost = 0;
        for (Service service : selectedServices) {
            totalCost += service.getPrice();
        }
        return totalCost;
    }

    public static void editReservationWorkstage() {
        consoleUI.showRecentReservation(reservationList);
        System.out.print("\nSilahkan Masukkan Reservation Id: ");
        String reservationId = ValidationService.validateStringInput();

        Reservation selectedReservation = findReservationById(reservationList, reservationId);
        if (selectedReservation == null) {
            System.out.println("Reservation yang dicari tidak tersedia.");
            return;
        }

        if (!selectedReservation.getWorkstage().equalsIgnoreCase("In process")) {
            System.out.println("Reservation yang dicari sudah selesai.");
            return;
        }

        System.out.print("Selesaikan reservasi (Finish/Cancel): ");
        String workstage = ValidationService.validateStringInput();
        if (workstage.equalsIgnoreCase("Finish") || workstage.equalsIgnoreCase("Cancel")) {
            selectedReservation.setWorkstage(workstage);
            System.out.println("Reservasi dengan ID " + reservationId + " sudah " + workstage);
        } else {
            System.out.println("Maaf, input Anda salah! Silakan masukkan 'Finish' atau 'Cancel'.");
        }
    }

    // Silahkan tambahkan function lain, dan ubah function diatas sesuai kebutuhan
}
