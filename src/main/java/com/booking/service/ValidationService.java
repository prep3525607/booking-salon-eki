package com.booking.service;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ValidationService {
    // Buatlah function sesuai dengan kebutuhan
    public static void validateInput() {

    }

    public static void validateCustomerId() {

    }

    public static String regexHuruf = "^[a-zA-Z ]+$";

    public static int numberProg = 1;

    public static String validateStringInput() {
        Scanner scanner = new Scanner(System.in);
        String input = "";
        boolean isValid = false;

        do {
            try {
                input = scanner.nextLine();
                isValid = true;
            } catch (Exception e) {
                System.out.println("Input tidak valid. Silakan coba lagi.");
                scanner.nextLine();
            }
        } while (!isValid);

        return input;
    }

    public static int validateIntegerInput() {
        Scanner scanner = new Scanner(System.in);
        int input = 0;
        boolean isValid = false;

        do {
            try {
                input = scanner.nextInt();
                isValid = true;
            } catch (InputMismatchException e) {
                System.out.println("Input harus berupa angka. Silakan coba lagi.");
                scanner.nextLine();
            }
        } while (!isValid);

        return input;
    }

    public static double validateDoubleInput() {
        Scanner scanner = new Scanner(System.in);
        double input = 0;
        boolean isValid = false;

        do {
            try {
                input = scanner.nextDouble();
                isValid = true;
            } catch (InputMismatchException e) {
                System.out.println("Input harus berupa angka double. Silakan coba lagi.");
                scanner.nextLine();
            }
        } while (!isValid);

        return input;
    }

    public static int inputRange(int min, int max) {
        Scanner scanner = new Scanner(System.in);
        int value;
        do {
            while (!scanner.hasNextInt()) {
                System.out.println("Input harus berupa angka.");
                scanner.next();
            }
            value = scanner.nextInt();
            if (value < min || value > max) {
                System.out.println("Input harus di antara " + min + " dan " + max + ".");
            }
        } while (value < min || value > max);
        return value;
    }

    public static String getUniqueID(String format) {
        String numberFormat = "";
        numberFormat = formatNumber(numberProg);
        numberProg++;

        return format + numberFormat;
    }

    public static String formatNumber(int number) {
        if (number < 10) {
            return "0" + number;
        } else {
            return Integer.toString(number);
        }
    }

    public static boolean inputBoolean() {
        Scanner scanner = new Scanner(System.in);
        boolean value;

        do {
            while (!scanner.hasNextBoolean()) {
                System.out.println("Input harus berupa true atau false.");
                scanner.next();
            }
            value = scanner.nextBoolean();
        } while (false);

        return value;
    }
}
