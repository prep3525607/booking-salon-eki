package com.booking.service;

import java.util.List;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.models.Service;
import com.booking.repositories.PersonRepository;

public class PrintService {

    private static final List<Person> persons = PersonRepository.getAllPerson();

    public static void printMenu(String title, String[] menuArr) {
        int num = 1;
        System.out.println(title);
        for (int i = 0; i < menuArr.length; i++) {
            if (i == (menuArr.length - 1)) {
                num = 0;
            }
            System.out.println(num + ". " + menuArr[i]);
            num++;
        }
    }

    public String printServices(List<Service> serviceList) {
        String result = "";
        // Bisa disesuaikan kembali
        for (Service service : serviceList) {
            result += service.getServiceName() + ", ";
        }
        return result;
    }

    // Function yang dibuat hanya sebgai contoh bisa disesuaikan kembali
    public void showRecentReservation(List<Reservation> reservationList) {
        int num = 1;
        System.out.println("+====================================================================================================================+");
        System.out.printf("| %-4s | %-4s | %-11s | %-35s | %-15s | %-15s | %-10s |\n",
                "No.", "ID", "Nama Customer", "Service", "Biaya Service", "Pegawai", "Workstage");
        System.out.println("+====================================================================================================================+");
        for (Reservation reservation : reservationList) {
            if (reservation.getWorkstage().equalsIgnoreCase("Waiting")
                    || reservation.getWorkstage().equalsIgnoreCase("In process")) {
                System.out.printf("| %-4s | %-4s | %-11s | %-35s | %-15s | %-15s | %-10s |\n",
                        num, reservation.getReservationId(), reservation.getCustomer().getName(),
                        printServices(reservation.getServices()), reservation.getReservationPrice(),
                        reservation.getEmployee().getName(), reservation.getWorkstage());
                num++;
            }
        }
        System.out.println("+====================================================================================================================+");
        
    }

    public void showAllCustomer() {

        int num = 1;
        System.out.println("+===============================================================================+");
        System.out.printf("| %-4s | %-8s | %-15s | %-15s | %-10s | %-10s |\n",
                "No.", "ID", "Nama Customer", "Alamat", "Membership", "Uang");
        System.out.println("+===============================================================================+");
        for (Person person : persons) {
            if (person instanceof Customer) {
                Customer customer = (Customer) person;
                System.out.printf("| %-4s | %-8s | %-15s | %-15s | %-10s | %-10s |\n",
                        num, customer.getId(), customer.getName(), customer.getAddress(),
                        customer.getMember().getMembershipName(), customer.getWallet());
                num++;
            }
        }
        System.out.println("+===============================================================================+");
    }

    public void showAllEmployee() {

        int num = 1;
        System.out.println("+=============================================================+");
        System.out.printf("| %-4s | %-8s | %-10s | %-15s | %-10s |\n",
                "No.", "ID", "Nama", "Alamat", "Pengalaman");
        System.out.println("+=============================================================+");
        for (Person person : persons) {
            if (person instanceof Employee) {
                Employee employee = (Employee) person;
                System.out.printf("| %-4s | %-8s | %-10s | %-15s | %-10s |\n",
                        num, employee.getId(), employee.getName(), employee.getAddress(), employee.getExperience());
                num++;
            }
        }
        System.out.println("+=============================================================+");
    }

    public void showHistoryReservation(List<Reservation> reservationList) {
        int num = 1;
        System.out.println("+==================================================================================================+");
        System.out.printf("| %-4s | %-4s | %-11s | %-35s | %-15s | %-10s |\n",
                "No.", "ID", "Nama Customer", "Service", "Total Biaya", "Workstage");
        System.out.println("+==================================================================================================+");
        double totalProfit = 0;
        for (Reservation reservation : reservationList) {
            if (reservation.getWorkstage().equalsIgnoreCase("Finish")) {
                totalProfit += reservation.getReservationPrice();
            }
            
            System.out.printf("| %-4s | %-4s | %-11s | %-35s | %-15s | %-10s |\n",
                    num, reservation.getReservationId(), reservation.getCustomer().getName(),
                    printServices(reservation.getServices()), reservation.getReservationPrice(),
                    reservation.getWorkstage());
            num++;
        }
        System.out.println("+==================================================================================================+");
        System.out.printf("| %-4s  %-4s  %-16s  %-25s  %-15s | %-10s | \n",
        "Total Keuntungan", "", "", "", "", (int) totalProfit);
        System.out.println("+==================================================================================================+");
    }
}
